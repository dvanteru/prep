﻿using System;

namespace prep.infrastructure.ranges
{
    public class FromToRange<T> : IContainValues<T> where T : IComparable<T>
    {
        private IContainValues<T> from;
        private T to_value;
        private bool to_inclusive;

        public FromToRange(IContainValues<T> from, T to_value, bool to_inclusive)
        {
            this.from = from;
            this.to_value = to_value;
            this.to_inclusive = to_inclusive;
        }

        public IContainValues<T> inclusive
        {
            get { return new FromToRange<T>(from,to_value,true); }
        }

        public bool contains(T value)
        {
            bool from_contains = from.contains(value);
            bool to_contains = to_inclusive ? value.CompareTo(to_value) <= 0 : value.CompareTo(to_value) < 0;

            return from_contains && to_contains;
        }
    }
}
