﻿using System;

namespace prep.infrastructure.ranges
{
    class InvertRange<T> : IContainValues<T> where T : IComparable<T>
    {
        private IContainValues<T> range;

        public InvertRange(IContainValues<T> range)
        {
            this.range = range;
        }

        public bool contains(T value)
        {
            return !range.contains(value);
        }
    }
}
