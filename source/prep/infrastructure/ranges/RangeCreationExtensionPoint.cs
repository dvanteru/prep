﻿using System;

namespace prep.infrastructure.ranges
{
  public class RangeCreationExtensionPoint<T> :IContainValues<T> where T : IComparable<T>
  {
    public T initial_value { get; set; }

    public RangeCreationExtensionPoint(T initial_value = default(T))
    {
      this.initial_value = initial_value;
    }

    public bool contains(T value)
    {
      return AlternateRange.from(initial_value).contains(value);
    }
  }
}