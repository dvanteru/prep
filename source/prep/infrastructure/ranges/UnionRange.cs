﻿using System;

namespace prep.infrastructure.ranges
{
    class UnionRange<T> : IContainValues<T> where T : IComparable<T>
    {
        private IContainValues<T> left;
        private IContainValues<T> right;

        public UnionRange(IContainValues<T> left, IContainValues<T> right)
        {
            this.left = left;
            this.right = right;
        }

        public bool contains(T value)
        {
            return left.contains(value) || right.contains(value);
        }
    }
}
