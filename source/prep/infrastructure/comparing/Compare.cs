﻿using System;
using System.Collections.Generic;
using prep.infrastructure.filtering;

namespace prep.infrastructure.comparing
{
  public class Compare<ItemToCompare>
  {
    public static IComparer<ItemToCompare> by_descending<PropertyToCompare>(PropertyAccessor<ItemToCompare, PropertyToCompare> accessor)
      where PropertyToCompare : IComparable<PropertyToCompare>
    {
      return by(accessor).reverse();
    }

    public static IComparer<ItemToCompare> by<PropertyToCompare>(PropertyAccessor<ItemToCompare,PropertyToCompare> accessor) 
      where PropertyToCompare : IComparable<PropertyToCompare>
    {
      return by(accessor, new ComparableComparer<PropertyToCompare>());
    }

    public static IComparer<ItemToCompare> by<PropertyToCompare>(PropertyAccessor<ItemToCompare,PropertyToCompare> accessor,
      params PropertyToCompare[] fixed_order) 
    {
      return by(accessor, new FixedComparer<PropertyToCompare>(fixed_order));
    }

    static IComparer<ItemToCompare> by<PropertyToCompare>(PropertyAccessor<ItemToCompare, PropertyToCompare> accessor,
                                                          IComparer<PropertyToCompare> value_comparer)
    {
      return new PropertyComparer<ItemToCompare, PropertyToCompare>(accessor, value_comparer);
    }
  }
}