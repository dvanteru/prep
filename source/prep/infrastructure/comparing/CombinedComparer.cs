﻿using System.Collections.Generic;

namespace prep.infrastructure.comparing
{
  public class CombinedComparer<ItemToCompare> : IComparer<ItemToCompare>
  {
    IComparer<ItemToCompare> first;
    IComparer<ItemToCompare> second;

    public CombinedComparer(IComparer<ItemToCompare> first, IComparer<ItemToCompare> second)
    {
      this.first = first;
      this.second = second;
    }

    public int Compare(ItemToCompare x, ItemToCompare y)
    {
      var result = first.Compare(x, y);
      return result == 0 ? second.Compare(x, y) : result;
    }
  }
}