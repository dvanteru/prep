﻿namespace prep.infrastructure.filtering
{
  public class OrMatch<ItemToMatch> : IMatchA<ItemToMatch>
  {
    IMatchA<ItemToMatch> first;
    IMatchA<ItemToMatch> second;

    public OrMatch(IMatchA<ItemToMatch> first, IMatchA<ItemToMatch> second)
    {
      this.first = first;
      this.second = second;
    }

    public bool matches(ItemToMatch item)
    {
      return first.matches(item) || second.matches(item);
    }
  }
}