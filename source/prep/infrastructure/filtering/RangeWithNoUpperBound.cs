﻿using System;
using prep.infrastructure.ranges;

namespace prep.infrastructure.filtering
{
  public class RangeWithNoUpperbound<T> : IContainValues<T> where T : IComparable<T>
  {
    T start;

    public RangeWithNoUpperbound(T start)
    {
      this.start = start;
    }

    public bool contains(T value)
    {
      return value.CompareTo(start) > 0;
    }
  }
}