﻿namespace prep.infrastructure.filtering
{
  public interface IMatchA<ItemToMatch>
  {
    bool matches(ItemToMatch item);
  }
}