﻿namespace prep.infrastructure.filtering
{
  public delegate bool Condition<ItemToMatch>(ItemToMatch item);
}