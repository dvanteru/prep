﻿namespace prep.infrastructure.filtering
{
  public interface IProvideAccessToMatchCreationExtensions<ItemToMatch, PropertyType>
  {
    IMatchA<ItemToMatch> create_matcher(IMatchA<PropertyType> value_matcher);
  }
}
