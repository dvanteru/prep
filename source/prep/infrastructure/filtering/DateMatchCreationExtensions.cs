﻿using System;

namespace prep.infrastructure.filtering
{
  public static class DateMatchCreationExtensions
  {
    public static IMatchA<ItemToMatch> greater_than<ItemToMatch>(
      this IProvideAccessToMatchCreationExtensions<ItemToMatch, DateTime> extension_point, int value)
    {
      return extension_point.create_matcher(MatchA<DateTime>.having_a(x => x.Year).greater_than(value));
    }
  }
}
